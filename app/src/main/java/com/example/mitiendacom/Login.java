package com.example.mitiendacom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.Toast;

import com.example.mitiendacom.bd.DbUsuarios;
import com.google.android.material.button.MaterialButton;

public class Login extends AppCompatActivity {

    EditText username, password;
    Button btnIniciarSesion;
    DbUsuarios DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion2);

        getSupportActionBar().hide();

        username = findViewById(R.id.usernameI);
        password = findViewById(R.id.passwordI);
        DB = new DbUsuarios(this);

        btnIniciarSesion =  findViewById(R.id.btnIniciarSesion);

        btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String user = username.getText().toString();
                    String pass = password.getText().toString();

                    if(user.isEmpty() || pass.isEmpty()) {

                      Toast.makeText(Login.this, "Se requiere llenar los campos", Toast.LENGTH_SHORT).show();

                    }else{

                        boolean checkuserpass = DB.checkcontrasena(user,pass);

                        if(checkuserpass == true) {

                             Toast.makeText(Login.this, "Login correctamente", Toast.LENGTH_SHORT).show();

                             Intent intent = new Intent(getApplicationContext(), NavigationBar.class);
                             startActivity(intent);

                        }else {
                            Toast.makeText(Login.this, "Login fallido", Toast.LENGTH_SHORT).show();
                        }

                    }
                    // });


                    //  btnIniciarSesion.setOnClickListener(new View.OnClickListener() {

                    //         @Override
                    //              public void onClick(View v) {
                    //             if (username.getText().toString().equals("admin") && password.getText().toString().equals("admin")) {
                    //                Toast.makeText(MainActivity.this, "Usuario Registrado", Toast.LENGTH_SHORT).show();
                    //            } else
                    //              Toast.makeText(MainActivity.this, "Usuario No Registrado", Toast.LENGTH_SHORT).show();


                }

                //  });
                //   }


                //   }
            }
        );
    }
}