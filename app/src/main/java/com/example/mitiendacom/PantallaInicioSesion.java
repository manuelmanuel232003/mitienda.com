package com.example.mitiendacom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class PantallaInicioSesion extends AppCompatActivity {

    Button iniciarSesion;
    Button registrarUsuario;
    Button btnFragmento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);

        iniciarSesion=(Button) findViewById(R.id.iniciarSesion);
        registrarUsuario=(Button)findViewById(R.id.registrarUsuario);
        btnFragmento=(Button)findViewById(R.id.irFragmento);

        getSupportActionBar().hide();

        iniciarSesion.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick (View v){

                Intent i = new Intent(PantallaInicioSesion.this, Login.class);
                startActivity(i);
            }

        });

        registrarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View a) {
                Intent y = new Intent(PantallaInicioSesion.this,RegistroUsuario.class);
                startActivity(y);
            }
        });

      
    }

}



