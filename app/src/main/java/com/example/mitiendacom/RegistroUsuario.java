package com.example.mitiendacom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mitiendacom.bd.DbUsuarios;

public class RegistroUsuario extends AppCompatActivity {

    EditText nombre, password, repassword, email1;
    Button btnRegistro, atras;
    DbUsuarios DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        getSupportActionBar().hide();


        nombre = findViewById(R.id.nombre);
        email1 = findViewById(R.id.email1);
        password = findViewById(R.id.password1);
        repassword = findViewById(R.id.rePassword);

        btnRegistro = findViewById(R.id.btnRegistro);
        atras = findViewById(R.id.atras);

        DB = new DbUsuarios(this);


        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = nombre.getText().toString();
                String pass = password.getText().toString();
                String email = email1.getText().toString();
                String repass = repassword.getText().toString();


                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(repass) || TextUtils.isEmpty(email)) {
                    Toast.makeText(RegistroUsuario.this, "Se requiere llenar los campos", Toast.LENGTH_SHORT).show();
                } else {
                    if (pass.equals(repass)) {
                        boolean checkuser = DB.checknomusuario(user);
                        if (checkuser == true) {
                            boolean insert = DB.insertarUsuario(user, pass);
                            if (insert == true) {
                                Toast.makeText(RegistroUsuario.this, "Registrado correctamente", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(RegistroUsuario.this, PantallaInicioSesion.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(RegistroUsuario.this, "Registro fallido", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(RegistroUsuario.this, "Registro exitoso", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(RegistroUsuario.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(RegistroUsuario.this, PantallaInicioSesion.class);
                startActivity(a);


            }
        });


        //  }
    }
}