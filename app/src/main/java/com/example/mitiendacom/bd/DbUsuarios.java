package com.example.mitiendacom.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

public class DbUsuarios extends DbHelper{

    Context context; // Variable global

    // CRUD
    // C = CREAR
    // R = LEER
    // U = ACTUALIZAR
    // D = ELIMINAR

    // Constructor
    public DbUsuarios(@Nullable Context context) {
        // SUPER: Llama al contructor de la clase padre
        super(context);
        this.context = context;
    }

    // Long: Es de tipo entero de mayor tamaño


    // NEW
    public boolean insertarUsuario(String nomusuario, String contrasena){
        DbHelper dbHelper = new DbHelper(context); // Intancia del objeto DbHleper = nuestra base de datos
        SQLiteDatabase db = dbHelper.getWritableDatabase(); // Agregamos los datos

        ContentValues values = new ContentValues(); // Instancia del objeto values
        values.put("nombre", nomusuario);
        values.put("password", contrasena);

        long result = db.insert(TABLE_USERS, null, values);
        if (result <= -1) {
            return false;
        }else {
            return true;
        }

    }

    public boolean checknomusuario(String nombre){
        DbHelper dbHelper = new DbHelper(context); // Instancia del objeto DbHelper
        SQLiteDatabase db = dbHelper.getWritableDatabase(); //
        // Colección de filas que son aleatorias
        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE nombre = ?", new String[] {nombre});
        if(cursor.getCount() == 0) {

            return false;

        }else {
            return true;
        }
    }

    public boolean checkcontrasena(String nomusuario, String contrasena){

        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE nombre = ? and password = ?", new String[] {nomusuario, contrasena});
        System.out.println(cursor.getCount());

        if(cursor.getCount() == 0) {
            System.out.println(cursor.getCount());

            return false;

        }else{
            return true;
        }
    }
}
